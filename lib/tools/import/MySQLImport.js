var mysql = require('mysql');
var datasourceUtils = require('../utils/DatasourceUtils');
var async = require('async');
var formatters = require('../utils/Formatters');
var sessionData = require("../../resource/SessionData.json");
var jsonfile = require('jsonfile');
var mysqlFolderName = "MySQL Data";
var path = require("path");
var _ = require("lodash");

function MySQLImport(mySQLPath, APICode, friendlyName, callback) {
    console.log("\n#################");
    console.log("MySQL Load ...")
    var connection = databaseConnection.connectToMySQL();
    //Locate and read datasource file (json)
    var readFile = function (APICode, friendlyName, next) {
        var datasourcePath = path.join(APICode, mysqlFolderName, APICode + " - " + friendlyName + ".json");
        jsonfile.readFile(datasourcePath, function (err, obj) {
            if (err) {
                console.log("- File : " + mySQLPath)
                jsonfile.readFile(mySQLPath, function (err, obj) {
                    if (err) {
                        console.error("Failed to read from file: ", mySQLPath);
                        console.error(err);
                        next(err);
                    } else {
                        next(null, obj);
                    }
                });
            }
            else {
                console.log("- File : " + datasourcePath)
                next(null, obj);
            }
        })
    }

    //Clears database tables in the file from bottom to top
    var clearTables = function (fileData, next) {
        async.each(_.reverse(fileData), function (data, cb) {
            deleteData(connection, Object.keys(data)[0], cb);
        }, function (err) {
            if (err) {
                console.error(err);
                next(err);
            }
            else {
                //send reverse of the array, to insert
                //data in the reverse order
                next(null, _.reverse(fileData));
            }
        });
    }

    var getColumnData = function (fileData, next) {

        var columnTypesArray = {};
        async.each(fileData, function (data, cb) {
            var table = Object.keys(data)[0];
            var tableSchema = table.split(".")[0];
            var tableName = table.split(".")[1];

            var sql = "SELECT COLUMN_NAME, DATA_TYPE FROM information_schema.COLUMNS where TABLE_SCHEMA = '" + tableSchema + "' and TABLE_NAME = '" + tableName + "'";

            connection.query(sql, function (err, columnType) {
                if (err) {
                    console.error(err);
                    cb(err);
                }
                var columnData = {};

                //retrieves the column name and type for each table
                for (var i = 0; i < columnType.length; i++) {
                    var key = columnType[i].COLUMN_NAME;
                    var value = columnType[i].DATA_TYPE;
                    columnData[key] = value;
                }
                //var tableData={};
                //tableData[table] = columnData;
                columnTypesArray[table] = columnData;
                cb();
            });

        }, function (err) {
            if (err) {
                next(err)
            }
            else {
                next(null, fileData, columnTypesArray);
            }
        })
    }

    var insertData = function (fileData, columnData, next) {
        async.each(fileData, function (data, tableCB) {
            var table = Object.keys(data)[0];
            var tableData = data[table];
            var columns = tableData.splice(0, 1)[0];

            async.each(tableData, function (row, rowCB) {
                insert(connection, table, row, columns, columnData[table], rowCB);
            }, function (err) {
                if (err) {
                    tableCB(err);
                }
                else {
                    console.log("insertData : " , table , " .. done");
                    tableCB();
                }
            })

        }, function (err) {
            if (err) {
                next(err);
            } else {
                next(null);
            }
        });
    }

    async.waterfall([async.apply(readFile, APICode, friendlyName), clearTables, getColumnData, insertData], function (err, results) {
        databaseConnection.disconnectMySQL(connection);
        if (err) {
            callback(err);
        }
        else {
            console.log("MySQL Load Complete.");
            callback();
        }
    })

}

function insert(connection, table, row, columns, columnTypes, cb) {
    var sql = "INSERT INTO " + table + " SET ?";
    var values = _.zipObject(columns,row)
    //console.log(sql)
    connection.query(sql, values, function (err, response) {
        if (err) {
            cb("Table :"+table+ " "+err);
        }
        else {
            cb();
        }
    });
}

function deleteData(connection, table, cb) {
    var sql = "DELETE FROM " + table;
    //console.log(sql)
    connection.query(sql, function (err, response) {
        if (err) {
            cb("Table :"+table+ " "+err);
        }
        else {
            cb();
        }
    });
}

module.exports = MySQLImport;
