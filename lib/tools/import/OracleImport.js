var async = require('async');
var jsonfile = require('jsonfile');
var oracleFolderName = "Oracle Data";
var path = require("path");
var _ = require("lodash");
var formatters = require('../utils/Formatters');

function OracleImport(APICode, friendlyName, callback) {

    //Locate and read datasource file (json)
    var readFile = function (APICode, friendlyName, next) {

        var datasourcePath = path.join(testRoot, APICode, oracleFolderName, APICode + " - " + friendlyName + ".json");
        jsonfile.readFile(datasourcePath, function (err, obj) {
            if (err) {
                next(err)
            }
            else {
                next(null, obj);
            }
        })
    }

    //Clears database tables in the file from top to bottom
    var clearTables = function (fileData, next) {
        async.each(fileData, function (data, cb) {
            var dbName = Object.keys(data)[0].split(".")[0];
            var table = Object.keys(data)[0].split(".")[1];

            databaseConnection.connectToOracle(function (err, db) {
                if (err) {
                    cb(err)
                }
                else {
                    deleteData(db, table, cb);
                }
            });
        }, function (err) {
            if (err) {
                next(err);
            }
            else {
                next(null, fileData);
            }
        });
    }

    var getColumnData = function (fileData, next) {

        var columnTypesArray = {};
        async.each(fileData, function (data, cb) {
            var table = Object.keys(data)[0];
            var tableSchema = table.split(".")[0];
            var tableName = table.split(".")[1];

            var sql = "SELECT COLUMN_NAME, DATA_TYPE FROM information_schema.COLUMNS where TABLE_SCHEMA = '" + tableSchema + "' and TABLE_NAME = '" + tableName + "'";

            connection.query(sql, function (err, columnType) {
                if (err) {
                    console.log(err);
                    cb(err);
                }
                var columnData = {};

                //retrieves the column name and type for each table
                for (var i = 0; i < columnType.length; i++) {
                    var key = columnType[i].COLUMN_NAME;
                    var value = columnType[i].DATA_TYPE;
                    columnData[key] = value;
                }
                //var tableData={};
                //tableData[table] = columnData;
                columnTypesArray[table] = columnData;
                cb();
            });

        }, function (err) {
            if (err) {
                console.log(err);
                next(err)
            }
            else {
                next(null, fileData, columnTypesArray);
            }
        })
    }

    var insertData = function (fileData, next) {
        async.each(fileData, function (data, tableCB) {
            var dbName = Object.keys(data)[0].split(".")[0];
            var tableName = Object.keys(data)[0].split(".")[1];
            var table = Object.keys(data)[0];
            var tableData = data[table];

            databaseConnection.connectToOracle(function (err, db) {
                if (err) {
                    cb(err)
                }
                else {

                    insert(db, tableName, tableData, tableCB);
                }
            });

        }, function (err) {
            if (err) {
                next(err);
            } else {
                next(null);
            }
        });
    }

    async.waterfall([async.apply(readFile, APICode, friendlyName), clearTables, insertData], function (err, results) {
        if (err) {
            console.log(err);
            callback(err);
        }
        else {
            console.log("Oracle Load Complete.");
            callback();
        }
    })

}

function deleteData(db, table, cb) {
    db.execute("DELETE FROM :db.:table",
        [db, table],
        function (err, result) {
            if (err) {
                cb(err);
            } else {
                cb();
            }
        });
}

function insert(db, table, data, cb) {

    db.execute("",[], function (err, result) {
        databaseConnection.disconnectOracle(db);
        if (err) {
            cb(err)
        }
        else {
            cb();
        }
    });
}

module.exports = OracleImport;