var async = require('async');
var jsonfile = require('jsonfile');
var mongoFolderName = "MongoDB Data";
var path = require("path");
var _ = require("lodash");

function MongoImport(mongoDBPath, APICode, friendlyName, callback) {

    //Locate and read datasource file (json)
    var readFile = function (APICode, friendlyName, next) {

        var datasourcePath = path.join(testRoot, APICode, mongoFolderName, APICode + " - " + friendlyName + ".json");
        jsonfile.readFile(datasourcePath, function (err, obj) {
            if (err) {
                console.log("Reading from Mongo PATH: ", mongoDBPath);
                jsonfile.readFile(mongoDBPath, function (err, obj) {
                    if (err) {
                        console.log("Failed to read from file: ", mongoDBPath);
                        console.log(err);
                        next(err);
                    } else {
                        next(null, obj);
                    }
                });
            }
            else {
                next(null, obj);
            }
        })
    }

    //Clears database tables in the file from top to bottom
    var clearTables = function (fileData, next) {
        async.each(fileData, function (data, cb) {
            var dbName = Object.keys(data)[0].split(".")[0];
            var collection = Object.keys(data)[0].split(".")[1];

            databaseConnection.connectToMongo(dbName, function (err, db) {
                if (err) {
                    cb(err)
                }
                else {
                    deleteData(db, collection, cb);
                }
            });
        }, function (err) {
            if (err) {
                next(err);
            }
            else {
                next(null, fileData);
            }
        });
    }

    var insertData = function (fileData, next) {
        async.each(fileData, function (data, tableCB) {
            var dbName = Object.keys(data)[0].split(".")[0];
            var collection = Object.keys(data)[0].split(".")[1];
            var table = Object.keys(data)[0];
            var tableData = data[table];

            databaseConnection.connectToMongo(dbName, function (err, db) {
                if (err) {
                    cb(err)
                }
                else {

                    insert(db, collection, tableData, tableCB);
                }
            });

        }, function (err) {
            if (err) {
                next(err);
            } else {
                next(null);
            }
        });
    }

    async.waterfall([async.apply(readFile, APICode, friendlyName), clearTables, insertData], callback)

}

function deleteData(db, collection, cb) {
    db.collection(collection).remove({}, function (err, num) {
        databaseConnection.disconnectMongo(db);

        if (err) {
            cb(err)
        }
        else {
            cb();
        }
    });
}

function insert(db, collection, data, cb) {

    db.collection(collection).insertMany(data, function (err, result) {
        databaseConnection.disconnectMongo(db);
        if (err || result.result.errmsg) {
            cb(err || result.result.errmsg)
        }
        else {
            cb();
        }
    });
}

module.exports = MongoImport;
