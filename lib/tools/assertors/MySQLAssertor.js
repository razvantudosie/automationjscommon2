var mysql = require('mysql');
var assert = require('chai').assert;
var formatters = require('../utils/Formatters');
var mysqlFolderName = "MySQL Data/Expected Data";
var path = require("path");
var _ = require("lodash");
var jsonfile = require('jsonfile');
var async = require('async');

//TODO: write this in a more NodeJS friendly way and check for possible errors
function getCorrespondingObject(obj, testCondition) {
    if(testCondition !== null) {
        var res = null;
        for(var test in obj) {
            if(typeof obj[test][testCondition] !== 'undefined') {
                res = obj[test][testCondition];
            }
        }
        return res;
    }
    else {
        return obj;
    }
}

function MySQLAssertor(mySQLPath, APICode, friendlyName, testCondition, callback) {

    console.log("\n#################");
    console.log("MySQLAssertor ...");

    var connection = databaseConnection.connectToMySQL();

    //Locate and read mysql expected data file (json)
    var readFile = function (APICode, friendlyName, next) {

        var datasourcePath = path.join(testRoot, APICode, mysqlFolderName, APICode + " - " + friendlyName + ".json");
        jsonfile.readFile(datasourcePath, function (err, obj) {
            if (err) {
                // console.log("- MySQL Assertor file : " + mySQLPath);
                jsonfile.readFile(mySQLPath, function (err, obj) {
                    if (err) {
                        // console.error("Failed to read from file: ", mySQLPath);
                        // console.error(err);
                        next(err);
                    } else {
                        parsedObj = getCorrespondingObject(obj, testCondition);
                        if(parsedObj === null)
                            next(new Error("Missing Test Condition: '" + testCondition + "' in Expected data JSON"));
                        next(null, parsedObj);
                    }
                });
            }
            else {
                // console.log("- MySQL Assertor file : " + mySQLPath);
                parsedObj = getCorrespondingObject(obj, testCondition);
                if(parsedObj === null)
                    next(new Error("Missing Test Condition: '" + testCondition + "' in Expected data JSON"));
                next(null, parsedObj);
            }
        })
    };

    var getColumnData = function (fileData, next) {

        var columnTypesArray = {};
        async.each(fileData, function (data, cb) {
            var table = Object.keys(data)[0];
            var tableSchema = table.split(".")[0];
            var tableName = table.split(".")[1];

            var sql = "SELECT COLUMN_NAME, DATA_TYPE FROM information_schema.COLUMNS where TABLE_SCHEMA = '" + tableSchema + "' and TABLE_NAME = '" + tableName + "'";

            connection.query(sql, function (err, columnType) {
                if (err) {
                    console.log(err);
                    cb(err);
                }
                var columnData = {};

                //retrieves the column name and type for each table
                for (var i = 0; i < columnType.length; i++) {
                    var key = columnType[i].COLUMN_NAME;
                    var value = columnType[i].DATA_TYPE;
                    columnData[key] = value;
                }
                //var tableData={};
                //tableData[table] = columnData;
                columnTypesArray[table] = columnData;
                cb();
            });

        }, function (err) {
            if (err) {
                next(err)
            }
            else {
                next(null, fileData, columnTypesArray);
            }
        })
    };

    var getTableData = function (fileData, columnData, next) {
        var tableData = {};
        async.each(fileData, function (data, cb) {
            var table = Object.keys(data)[0];
            var tableSchema = table.split(".")[0];
            var tableName = table.split(".")[1];

            var sql = "SELECT * FROM " + table;


            connection.query(sql, function (err, data) {
                if (err) {
                    cb(err);
                }
                // convert the result data in JSON
                var string=JSON.stringify(data);
                tableData[table] =  JSON.parse(string);

                cb();
            });

        }, function (err) {
            if (err) {
                next(err)
            }
            else {
                next(null, fileData, tableData);
            }
        })
    };

    var assertTableData = function (fileData, tableData, next) {

        //For each table, we ensure that the corresponding properties are equal, in that order
        async.each(fileData, function (data, cb) {
            var table = Object.keys(data)[0];
            var tableSchema = table.split(".")[0];
            var tableName = table.split(".")[1];

            console.log("Asserting Table: " + table);

            //first we compare the length of the table and expected data
            if (data[table].length - 1 != tableData[table].length) {
                next(new Error("Length of expected table data ("+table+") and actual table data do not match. ( expct : "
                    + (data[table].length - 1) + " -vs- found : " + tableData[table].length + " )"));
            } else {
                // compare the values
                var errors = [];
                // for each row from data base
                for (i = 0; i < tableData[table].length; i++) {
                    var row = tableData[table][i];

                    // for each 'column' from expected data:
                    for (j = 0; j < _.size(data[table][0]); j++) {

                        //console.log("Actual = " + row[data[table][0][j]])
                        //console.log("Expect = " + data[table][i+1][j])

                        // ignore '*'
                        if(data[table][i+1][j] === "*")
                            continue;

                        if (row[data[table][0][j]] != data[table][i+1][j]){
                            errors.push("[tabel : " + table + "][row : " + i + "] : <" +data[table][0][j] +"> : ( found : " + row[data[table][0][j]] +
                                        " != expect : " + data[table][i+1][j] + " )");
                        }
                    }
                }
                if (errors.length > 0 ) {
                    console.log("MySQL Assert Failed : values do not match : ");
                    next(new Error(errors));
                } else {
                    cb();
                }
            }

        }, function (err) {
            if (err) {
                next(err)
            }
            else {
                next(null, fileData, tableData);
            }
        })
    };

    async.waterfall([async.apply(readFile, APICode, friendlyName), getColumnData, getTableData, assertTableData], function (err, results) {
        databaseConnection.disconnectMySQL(connection);
        if (err) {
            console.log("err:",err);
            callback(err);
        }
        else {
            console.log("MySQL Assert Complete.");
            console.log("No errors");
            console.log("#################\n");
            callback();
        }
    })
}
/*
MySQLAssertor.prototype.fromFile = function(file, cb) {
    async.waterfall([
     datasourceUtils.toMap.bind(this, file),
     function(datasourceMap, cb) {
        Object.keys(datasourceMap).forEach(function(table) {
            this.getData(table, function(err, rows, fields) {
                if(err) {
                    return cb(err);
                }

                this.assert(datasourceMap[table].splice(0, 1), rows);
            })
        });
     }
    ], cb);
}

MySQLAssertor.prototype.getData = function(table, cb) {
    this.connection.query('SELECT * FROM ' + table, cb);
}

MySQLAssertor.prototype.assert = function(actual, expected, cb) {
    assert.lengthOf(actual.length, expected.length);

    for(let i = 0; i < actual.length; i++) {
        actual[i] = actual[i].map(function(cell) {
            return formatters.format(cell);
        });

        assert.deepEqual(actual[i], expected[i]);
    }
}*/


module.exports = MySQLAssertor;
