var csv = require('csv-parse/lib/sync');
var csvStringify = require('csv-stringify');
var fs = require('fs');
var _ = require('lodash');

/**
 * Read-only XLSX store
 * 
 * @param xlsxPath path to xlsx/xls file
 */
function CsvStore(csvPath) {
    this.path = csvPath;
    this.file = csv(fs.readFileSync(csvPath, { encoding: 'utf8' }));
}

CsvStore.prototype.findWorkbookSheetByName = function(sheets, name) {
    return this.file;
};

CsvStore.prototype.getFirstWorkbookSheet = function() {
    return this.file;
};

CsvStore.prototype.getRow = function(sheet, i) {
    return this.file[i];
}

CsvStore.prototype.getHeaderMap = function(sheet) {
    var headerMap = {};
    for (var i = 0; i < sheet[0].length; i++) {
       headerMap[sheet[0][i]] = i;
    }

    return headerMap;
};

CsvStore.prototype.getHeaderArray = function(sheet) {
    return this.getRow(null, 0);
}

CsvStore.prototype.getAllValuesForColumn = function(sheet, columnKey) {
    return sheet.reduce(function(prev, cur, i) {
        if(i !== 0) {
            prev.push(cur[columnKey]);
        }
        
        return prev;
    }, []);
};

CsvStore.prototype.sheetToArray = function(sheet) {
    return sheet;
}

CsvStore.prototype.updateSpreadsheet = function(sheet, row, i) {
    this.file[i] = row;
}

CsvStore.prototype.save = function(cb) {
    var that = this;
    
    csvStringify(this.file, function(err, output) {
        if(err) {
            return cb(err);
        }
        
        fs.writeFile(that.path, output, 'utf8', cb);
    });
}

module.exports = CsvStore;