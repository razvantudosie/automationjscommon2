var xlsx = require('xlsx');
var _ = require('lodash');

/**
 * Read-only XLSX store
 * 
 * @param xlsxPath path to xlsx/xls file
 */
function XlsxStore(xlsxPath) {
    this.file = xlsx.readFile(xlsxPath);
}

XlsxStore.prototype.findWorkbookSheetByName = function(sheets, name) {
    var sheetNames = _.keys(sheets);
    for (var i = 0; i < sheetNames.length; i++) {
        var sheetName = sheetNames[i];
        if (sheetName.trim().toLowerCase() === name.trim().toLowerCase()) {
            return sheets[sheetName];
        }
    }

    return null;
};

XlsxStore.prototype.getFirstWorkbookSheet = function() {
    var sheetNames = _.keys(this.file.Sheets);
    
    for (var i = 0; i < sheetNames.length; i++) {
        var sheetName = sheetNames[i];

        return this.file.Sheets[sheetName];
    }
};

XlsxStore.prototype.getHeaderMap = function(sheet) {
    var headerMap = {};
    for (var k in sheet) {
        if (k === "!ref")
            continue;

        var rowNumber = k.substring(k.search(/\d/));
        if (rowNumber === "1") {
            headerMap[sheet[k].v] = k;
        }
    }

    return headerMap;
};

XlsxStore.prototype.getHeaderArray = function(sheet) {
    var headerArray = [];
    
    for(var k in sheet) {
        if(k === "!ref") {
            continue;
        }
        
        var rowNumber = k.substring(k.search(/\d/));
        if(rowNumber === "1") {
            headerArray.push(sheet[k].v);
        }
    }
    
    return headerArray;
}

XlsxStore.prototype.getAllValuesForColumn = function(sheet, columnKey) {
    columnKey = columnKey.substr(0, columnKey.search(/\d/));
    var values = [];

    var index = 2;
    while (true) {
        var cell = columnKey + index;
        var cellValue = sheet[cell];
        if (cellValue) {
            values.push(cellValue.v);
        } else {
            break;
        }

        index++;
    }

    return values;
};

XlsxStore.prototype.sheetToArray = function(sheet) {
    return xlsx.utils.sheet_to_json(sheet);
}

XlsxStore.prototype.updateSpreadsheet = function(sheet, row, i) {
    
}

module.exports = XlsxStore;