var _ = require("lodash");
var path = require("path");
var XlsxStore = require("./stores/xlsxStore");
var CsvStore = require("./stores/csvStore");

var MAX_SHEETNAME_LENGTH = 31;

var TestTypes = {
    "valid": "Valid",
    "invalid": "Invalid",
    "authentication": "Auth"
};

var URL_PARAMETER_PREFIX = "u_";
var QUERY_PARAMETER_PREFIX = "q_";
var POST_PARAMETER_PREFIX = "p_";

var DATASOURCE_FOLDER_NAME = "Datasources";

function trimFriendlyName(friendlyName) {
    return friendlyName.substr(0, MAX_SHEETNAME_LENGTH);
}

function getTestConditions(obj) {
    return tryAndObtain(obj, ["condition", "conditions",
        "test condition", "test conditions"]);
}

function getTestDescriptions(obj) {
    return obj["test description"] || null;
}

function getProjects(obj) {
    return obj["project"] || null;
}

function tryAndObtain(obj, keys) {
    for (var i = 0; i < keys.length; i++) {
        var val = obj[keys[i]];
        if (val !== undefined)
            return val;
    }
    return null;
}

function extractFeaturesFromPath(dataSourcePath, enabledSuites, cb) {
    var pathParts = dataSourcePath.split(path.sep);
    var dataSourceIndex = pathParts.indexOf(DATASOURCE_FOLDER_NAME);

    //We do not have a valid path...
    if (dataSourceIndex === -1) {
        console.error("Unable to find data source folder for excel file path: %s", dataSourcePath);

        return cb();
    }

    var fileName = pathParts[pathParts.length - 1];
    fileName = fileName.split(" - ");
    fileName = fileName[fileName.length - 1];

    fileName = fileName.split("\.");
    fileName = fileName[0].trim();

    var enabled = false;
    if (~(enabledSuites.indexOf(fileName))) {
        enabled = true;
    }

    //Find module name
    var moduleName = null;

    var esgApi = pathParts[dataSourceIndex - 1];
    var esgApiParts = esgApi.split(" - ");

    var esgId = esgApiParts.splice(0, 1)[0];

    if (!esgId) {
        console.error("Unable to find ESG ID for data source: %s", dataSourcePath);

        return cb();
    }

    var dataSourceType = "Unknown";

    var fileName = pathParts.splice(-1, 1)[0].toLowerCase();
    if (~(fileName.indexOf("invalid"))) {
        dataSourceType = "Invalid";
    } else if (~(fileName.indexOf("valid"))) {
        dataSourceType = "Valid";
    }

    return cb(null, {
        esgId: esgId,
        sheetName: esgApiParts.join(" - "),
        module: moduleName,
        testType: dataSourceType,
        filePath: dataSourcePath,
        enabled: enabled
    });
}

function getSlugHeaderMap(tempMap) {
    var slugMap = {};
    for (var k in tempMap) {
        slugMap[k.toLowerCase().trim()] = tempMap[k];
    }

    return slugMap;
}

function readFile(filePath) {
    if (filePath.indexOf(".csv", this.length - ".csv".length) !== -1) {
        return new CsvStore(filePath);
    }

    return new XlsxStore(filePath);
}

function extractSpreadsheetFromDatasourceAsArrays(datasourcePath) {
    var workbook = readFile(datasourcePath);

    var sheet = workbook.getFirstWorkbookSheet();

    return workbook.sheetToArray(sheet);
}

function extractTestDataFromDatasource(dataSourcePath, sheetName) {
    var workbook = readFile(dataSourcePath);

    var sheet = workbook.getFirstWorkbookSheet();
    if (!sheet) {
        console.error("Datasource spreadsheet is invalid, cannot find sheetname: " + sheetName + ", consider reviewing the test setup");

        return cb(null, []);
    }

    var headerMap = getSlugHeaderMap(workbook.getHeaderMap(sheet));

    var testConditionCell = getTestConditions(headerMap);
    var testConditionValues = [];

    if (testConditionCell !== undefined) {
        testConditionValues = workbook.getAllValuesForColumn(sheet, testConditionCell);
    }

    if (testConditionValues.length === 0) {
        console.error("Unable to find test conditions inside datasource file");

        return cb(null, []);
    }

    var testDescriptionCell = getTestDescriptions(headerMap);
    var testDescriptionValues = [];
    if (testDescriptionCell) {
        testDescriptionValues = workbook.getAllValuesForColumn(sheet, testDescriptionCell);
    }

    var projectCell = getProjects(headerMap);
    var projectValues = [];
    if (projectCell) {
        projectValues = workbook.getAllValuesForColumn(sheet, projectCell);
        projectValues = projectValues.map(function (projectValue) {
            if (!projectValue) {
                return "Masterplan";
            }

            return projectValue;
        });
    } else {
        for (var i = 0; i < testConditionValues.length; i++) {
            projectValues.push("Masterplan");
        }
    }

    return testConditionValues.map(function mapToObj(testCondition, i) {
        return {
            testCondition: testCondition,
            testDescription: testDescriptionValues[i],
            project: projectValues[i],
            row: i,
            datasourcePath: dataSourcePath
        };
    });
}

module.exports = {
    getTestConditions: getTestConditions,
    getTestDescriptions: getTestDescriptions,
    getProjects: getProjects,
    extractFeaturesFromPath: extractFeaturesFromPath,
    extractTestDataFromDatasource: extractTestDataFromDatasource,
    extractSpreadsheetFromDatasourceAsArrays: extractSpreadsheetFromDatasourceAsArrays,
    readFile: readFile
};