var chrono = require('chrono-node');

const FUNC_REGEX = "";

// function varchar(args) {

// }

// function date(args) {

// }

//cell = ${varchar:blah}

function format(cell, columnType) {
    // var mataches = cell.match(FUNC_REGEX);

    // if(matches) {
    //     var funcType = matches[1];
    //     var sss

    //     funcType(sss);
    // } 
    if (columnType == "datetime" || columnType == "date") {
        return formatDatetime(cell);
    }

    return "'" + cell + "'";
}

function formatDatetime(cell) {
    var response = chrono.parse(cell);
    if (response[0] != null) {
        //Returns Day Month Date Year + time
        var day = response[0].start.knownValues.day;
        var month = response[0].start.knownValues.month;
        var year = response[0].start.knownValues.year;
        return "'" + year + "-" + month + "-" + day + "'";
    }
    return "'" + cell + "'";
}

module.exports = {
    format: format
}